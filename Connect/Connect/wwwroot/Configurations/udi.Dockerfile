FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR udi-platform

EXPOSE 80
EXPOSE 5001

COPY ./*.csproj ./

COPY . .
RUN dotnet publish -c Release -o deploy

FROM mcr.microsoft.com/dotnet/sdk:5.0
WORKDIR /udi-platform
COPY --from=build /udi-platform/deploy .
ENTRYPOINT ["dotnet","Connect.dll"]
