
var config = new Object();
$(document).ready(function () {
    load_config();
    
});

/*

$(document).ready(function () {
    $(document).ajaxStart(function () {
        $('#bckgloader').removeClass('hide');
    }).ajaxStop(function () {
        $('#bckgloader').addClass('hide');
    });
});*/
//[   {     "storeId": 6202266,     "Merchant": "GEOPAGOS"   }]

function loadMCSRequest() {

    $('#bckgloader').removeClass('hide');
    var xmlInit = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'><soapenv:Header/><soapenv:Body><ns2:mcsRequest xmlns:ns2='http://www.ipg-online.com/mcsWebService'>";
    var xmlFin = "</ns2:mcsRequest></soapenv:Body></soapenv:Envelope>"

    var storeInput = $('#storeId').val();

    var JStores = JSON.parse(storeInput);

    var resultTable = [];
}

function CloneStores() {
    $('#bckgloader').removeClass('hide');
    var xmlInit = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'><soapenv:Header/><soapenv:Body><ns2:mcsRequest xmlns:ns2='http://www.ipg-online.com/mcsWebService'>";
    var xmlFin = "</ns2:mcsRequest></soapenv:Body></soapenv:Envelope>"

    var storeInput = $('#storeId').val();

    var JStores = JSON.parse(storeInput);

    var resultTable = [];
    JStores.map(function (store) {
        var JResult = {};
        var StoreId = store.storeId.toString();

        var Merchant = store.Merchant.toString();

        var reseller = GetResellerByStoreId(StoreId);

        var JsonReq = {
            "ns2:getStore": {
                "ns2:storeID": StoreId
            }
        }

        var xmlReq = OBJtoXML(JsonReq);


        xmlReq = xmlInit + xmlReq + xmlFin

        var addObj = new Object;

        addObj['RequestKey'] = 'Get'

        addObj['CloneStore'] = true

        addObj['cloneStoreId'] = store.Clone.toString();

        addObj['Reseller'] = reseller

        var CloneConfig = soapExec(xmlReq, addObj)

        if (CloneConfig.responseJSON) {

            if (CloneConfig.responseJSON.status == "GOT") {
                var cloneRequest = gatherForCreate(CloneConfig.responseJSON, StoreId)
            }
            addObj['RequestKey'] = 'Create'

            addObj['CloneStore'] = false

            var ClonedStore = soapExec(cloneRequest, addObj);

            JResult["StoreId"] = StoreId;

            JResult["MerchantName"] = Merchant;

            if (ClonedStore.responseJSON.status == "CLONED") {

                JResult["Status"] = "Clonado"
            }

            resultTable.push(JResult);
            console.log('El StoreId: ' + StoreId + ' ha sido clonado en el StoreId: ' + store.Clone.toString())

        }



    })

    console.table(resultTable)
    $('#bckgloader').addClass('hide');
}

function GetStoresByParam() {

    $('#bckgloader').removeClass('hide');
    var xmlInit = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'><soapenv:Header/><soapenv:Body><ns2:mcsRequest xmlns:ns2='http://www.ipg-online.com/mcsWebService'>";
    var xmlFin = "</ns2:mcsRequest></soapenv:Body></soapenv:Envelope>"

    var storeInput = $('#storeId').val();

    var JStores = JSON.parse(storeInput);

    var resultTable = [];


    JStores.map(function (store) {
        var JResult = {};
        var StoreId = store.storeId.toString();

        var Merchant = store.Merchant.toString();

        var keyParam = store.keyParam.toString();

        var reseller = GetResellerByStoreId(StoreId);

        var JsonReq = {
            "ns2:getStore": {
                "ns2:storeID": StoreId
            }
        }

        var xmlReq = OBJtoXML(JsonReq);


        xmlReq = xmlInit + xmlReq + xmlFin

        var addObj = new Object;

        addObj['RequestKey'] = 'Get'

        addObj['UpdateStore'] = false

        addObj['Reseller'] = reseller

        addObj['GetParam'] = keyParam

        var GotConfig = soapExec(xmlReq, addObj)

        if (GotConfig.responseJSON) {


            JResult["StoreId"] = StoreId;

            JResult["MerchantName"] = Merchant;

            JResult[keyParam] = GotConfig.responseJSON.gotParam;

            resultTable.push(JResult);
            console.log('El MID del storeId ' + StoreId + ' es ' + JResult[keyParam] )

        }



    })

    console.table(resultTable)
    $('#bckgloader').addClass('hide');
}


function UpdateStores() {

    $('#bckgloader').removeClass('hide');
    var xmlInit = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'><soapenv:Header/><soapenv:Body><ns2:mcsRequest xmlns:ns2='http://www.ipg-online.com/mcsWebService'>";

    var xmlFin = "</ns2:mcsRequest></soapenv:Body></soapenv:Envelope>"

    var storeInput = $('#storeId').val();

    var JStores = JSON.parse(storeInput);

    var resultTable = [];

   

    JStores.map(function (store) {
        var JResult = {};
        var StoreId = store.storeId.toString();

        var Merchant = store.Merchant.toString();

        var reseller = GetResellerByStoreId(StoreId);

        var JsonReq = {
            "ns2:getStore": {
                "ns2:storeID": StoreId
            }
        }

        var xmlReq = OBJtoXML(JsonReq);


        xmlReq = xmlInit + xmlReq + xmlFin

        var addObj = new Object;

        addObj['RequestKey'] = 'Get'

        addObj['UpdateStore'] = true

        addObj['Reseller'] = reseller

        var UpdatedConfig = soapExec(xmlReq, addObj)

        if (UpdatedConfig.responseJSON) {

            if (UpdatedConfig.responseJSON.status == "GOT") {
                var updateRequest = gatherForUpdate(UpdatedConfig.responseJSON, StoreId)
            }
            addObj['RequestKey'] = 'Update'

            addObj['UpdateStore'] = false

            var UpdatedStore = soapExec(updateRequest, addObj);

            JResult["StoreId"] = StoreId;

            JResult["MerchantName"] = Merchant;

            if (UpdatedStore.responseJSON.status == "UPDATED") {

                JResult["Status"]="Actualizado"
            }

            resultTable.push(JResult);
            console.log('StoreId: ' + StoreId+' ha sido actualizado')

        }

        

    })

    console.table(resultTable)
    $('#bckgloader').addClass('hide');
}

function GetResellerByStoreId(store) {
    var reseller;
    reseller_config.map(function (res) {
        var sufixLength = res.prefix.length;
        if (store.slice(0, sufixLength) == res.prefix) {
            reseller = res.reseller;
        }
    })
    return reseller;
}

function gatherForUpdate(params,store) {
    var xmlInit = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'><soapenv:Header/><soapenv:Body><ns2:mcsRequest xmlns:ns2='http://www.ipg-online.com/mcsWebService'>";

    var xmlFin = "</ns2:mcsRequest></soapenv:Body></soapenv:Envelope>"


    var updateParams = params.xmlResponse;

    var JsonReq = {
        "ns2:updateStore": updateParams
    }


    var xmlReq = OBJtoXML(JsonReq);

    xmlReq = xmlInit + xmlReq + xmlFin

    return xmlReq;

}

function gatherForCreate(params, store) {
    var xmlInit = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'><soapenv:Header/><soapenv:Body><ns2:mcsRequest xmlns:ns2='http://www.ipg-online.com/mcsWebService'>";

    var xmlFin = "</ns2:mcsRequest></soapenv:Body></soapenv:Envelope>"


    var createParams = params.xmlResponse;

    var JsonReq = {
        "ns2:createStore": createParams
    }


    var xmlReq = OBJtoXML(JsonReq);

    xmlReq = xmlInit + xmlReq + xmlFin

    return xmlReq;

}


function soapExec(params,addedObj) {

    var request = params;
    
    var result = $.ajax({
        type: 'POST',
        url: '/BoardingAPI/' + addedObj['RequestKey'] + 'Store',
        data: { "request": request, "update": addedObj['UpdateStore'], "reseller": addedObj['Reseller'], "keyParam": addedObj['GetParam'], "clone": addedObj['CloneStore'], "clonedStore": addedObj['cloneStoreId'] },
        dataType: 'json',
        async: false,
        ContentType: 'application/json',
        success: function (response) {
            setTimeout(function () {
                result = response
                
            }, 1100);
        }
    });
    
    return result
}
var reseller_config = getResellerConfig();

function load_config() {

    var obj;
    
    const jconf =
        fetch("/Configurations/StoreConfigurator.json")
            .then(response => {
                return response.json();
            })
            .then(data => obj = data)
            .then(() => config = obj);

}

function getResellerConfig() {

    var config = $.ajax({
        type: 'POST',
        url: '/BoardingAPI/ResellerConfig',
        dataType: 'json',
        async: false,
        ContentType: 'application/json',
        success: function (response) {

        }
    });
    return JSON.parse(config.responseJSON)


}

class StoreConfig extends React.Component {

    loadForm() {

    const [form, setForm] = useState({});

    const handleChange = e => {
        setForm({
            ...form, [e.target.name]: e.target.value
        })
    }

    const handleChecked = e => {
        setForm({
            ...form, [e.target.name]: e.target.checked
        })
    }

}

    render() {
        return  (<div>IField : Componente No Implementado.</div>);
    }

}

function OBJtoXML(obj) {
    var xml = '';
    for (var prop in obj) {
        xml += obj[prop] instanceof Array ? '' : "<" + prop + ">";
        if (obj[prop] instanceof Array) {
            for (var array in obj[prop]) {
                xml += "<" + prop + ">";
                xml += OBJtoXML(new Object(obj[prop][array]));
                xml += "</" + prop + ">";
            }
        } else if (typeof obj[prop] == "object") {
            xml += OBJtoXML(new Object(obj[prop]));
        } else {
            xml += obj[prop];
        }
        xml += obj[prop] instanceof Array ? '' : "</" + prop + ">";
    }
    var xml = xml.replace(/<\/?[0-9]{1,}>/g, '');
    return xml
}