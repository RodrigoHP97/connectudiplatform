
var sep_conf = {
    "HMACSHA256": {
        "separator": "",
        "controller": "HashHMAC",
        "name":"hashExtended"
    }
};


document.addEventListener('keypress', function (e) {
    if (e.key === 'Enter') {
        $("#sendData").click();
    }
});
$(document).ready(function () {
    $(document).ajaxStart(function () {
        $('#bckgloaderframe').removeClass('hide');
    }).ajaxStop(function () {
        $('#bckgloaderframe').addClass('hide');
    });
});
/*
$(document).ready(function () {
    $(document).ajaxStart(function () {
        $('#bckgloader').removeClass('hide');
    }).ajaxStop(function () {
        $('#bckgloader').addClass('hide');
    });
});*/
var setStore = false;
var setCof = false;
var setCrypto = false;
var jpost = new Object;
var list = [];
var post = new Object;
var repost = {}
var services_conf;
var version;
var patch = {};
var orderObj = new Object;
var redirect = new Object;
var onComplete = new Object;
var onSave = new Object;
var par_Obj = new Object;
var Req_Obj = new Object;
var final = {};
var auth;
var authParent;
var Sredirect = {};
var authpatch;
var referenceId;
var threeDsfunction;
var t_request = new Object;
var SplitComplete = new Object;
var postToken = false;
var create_token = false;
var AddReq = [];
var scheduleURL;
var scheduleRequest = [];

function sort_by_key(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}



function load_config() {
    create_token = false;
    auth = false;
    var postToken = false;
    $('#AdditionalReqTab').children().remove()
    $('#serviceTab').children().remove()
    $('#div_master').remove();
    const jconf =
        fetch("/Configurations/API.json")
            .then(response => {
                return response.json();
            })
            .then(data => { return Promise.resolve(data) });

    //Armamos formulario
    jconf.then((value) => {
        Req_Obj = {};
        var jval = $('#type_of_selling').children(":selected").attr("id");



        if (jval != 'keyvalidation') {

            var general_form = value.filter(function (case_config) {
                if (Object.keys(case_config)[0] == 'general') {
                    return case_config;
                }

            });

            value.filter(function (case_config) {
                if (Object.keys(case_config)[0] == 'partial') {
                    services_conf = case_config['partial'];
                }

            });
            var ignore;
            var form = value.filter(function (case_config) {
                if (Object.keys(case_config)[0] != 'general') {

                    return case_config;
                }

            });
            var types = form.filter(function (e) { return e[$('#type_of_selling').val()] })[0]
            var gen_Sort = types[$('#type_of_selling').val()].concat(general_form[0].general);
           
        }
        else {
            var general_form = value.filter(function (case_config) {
                if (Object.keys(case_config)[0] == 'keyvalidation') {
                    return case_config;
                }

            });
            var gen_Sort = (general_form[0].keyvalidation);
        }


        
        gen_Sort = sort_by_key(gen_Sort, "order");


            $('#buttonData').before('<div id="div_master"></div>');

            gen_Sort.map(function (value) {
                if (Object.keys(value)[0] != 'post' &&
                    Object.keys(value)[0] != 'redirect' &&
                    Object.keys(value)[0] != 'parentChild' &&
                    Object.keys(value)[0] != 'onComplete' &&
                    Object.keys(value)[0] != '3ds' &&
                    Object.keys(value)[0] != 'onSave' &&
                    Object.keys(value)[0] != 'services') {

                    list.push(value.name);
                    if (!value.deleteInputFromGeneral) {
                        input_loader(value)
                    }
                }
                else
                {
                    //Mapeamos checkbox de servicios
                    if (Object.keys(value)[0] == 'services') {
                        servicesMapper(value.services);
                    }
                    if (Object.keys(value)[0] == 'post') { jpost = value.post; pay_post = value.post;  }
                    if (Object.keys(value)[0] == 'parentChild') { Req_Obj = value.parentChild }
                    if (Object.keys(value)[0] == 'onComplete') { onComplete = value.onComplete }
                    if (Object.keys(value)[0] == 'onSave') { onSave = value.onSave }
                    if (Object.keys(value)[0] == 'redirect') {
                        Object.keys(value.redirect).map(function (ind) {
                            redirect[ind] = value.redirect[ind]
                        })
                    }
                }

            })

            $('#hash_algorithm').val($('#hash_action').val());

            if ($('#number').val() != undefined) {
                document.getElementById('number').addEventListener('input', function (e) {
                    e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
                });
            }
        });
}

function servicesMapper(serv) {

    serv.map(function (check) {
        if (services_conf) {
            services_conf.map(function (trans) {
                if (trans[check]) {
                    //construimos formulario escondido 
                    var checkbox = trans[check].form;
                    if (checkbox) {
                        checkbox.map(function (value) {
                            input_loader(value)

                        })
                        
                    }
                    if (trans[check].input) {
                        input_loader(trans[check].input)
                    }

                    if (trans[check].version) {
                        version = trans[check].version;
                    }

                    if (trans[check].redirect) {
                        
                        Sredirect[check]= trans[check].redirect
                    }
                }
            })
        }


    })

}

function scheduleNumofPaymentsGen() {
    var numOfpym = [];
    for (var i = 1; i <= 100; i++) {

        var numobj = new Object;
        numobj.id = i;
        numobj.label = i;
        numobj.value = i;
        numOfpym.push(numobj)
    }

    return numOfpym;
}

function scheduleNumofTimesGen() {
    var numOfpym = [];
    for (var i = 1; i <= 100; i++) {

        var numobj = new Object;
        numobj.id = i;
        numobj.label = i;
        numobj.value = i;
        numOfpym.push(numobj)
    }

    return numOfpym;
}


function input_loader(value, key) {

    var div2append;
    var col_adjustment;

    if (key) {

        div2append = key
        col_adjustment = ' '
        col_adjustment2= ' '

    }
    else {
        div2append = 'div_master'
        col_adjustment = 'col-md-4 col-xs-6 '
        col_adjustment2 = 'col-sm-12 '
    }


    if (value.label != undefined) {

        //input loader


        //validamos tipo de input
        if (value.type == "select") {

            if (value.subtype == 'month') {
                value.options = monthArrayLoader();

            }
            if (value.subtype == 'year') {
                value.options = yearArrayLoader();
            }
            if (value.name == 'numberOfPayments') {
                value.options = scheduleNumofPaymentsGen();
            }
            if (value.name == "every") {
                value.options = scheduleNumofTimesGen()
            }
            var div = '<div class="form-group ' + col_adjustment + value.class + '" master="'+value.master +'"  id="' + value.name + '_field"></div>';

            var label = '<label class="control-label "' + col_adjustment2+' id="' + value.name + '_label">' + value.label + '</label>';


            $('#' + div2append).append(div);
            var select = label + '<select class="form-control"  id="' + value.name + '" name="' + value.name + '"parent="' + value.parent + '" style="width: 200px"></select>';

            $('#' + value.name + '_field').append(select);



            value.options.map(function (values) {

                var option = '<option style="width: 200px" id="' + values.id + '"  value="' + values.value + '">' + values.label + '</option>';

                $('#' + value.name).append(option);


            })

            if (value.name == 'month') {
                var d = new Date(); 
                var m = d.getMonth() + 1
                var mS = m.toString()
                if (mS.length < 2) {
                    mS = '0' + mS;
                }
                $('#month').val(mS)
            }


        }


        else {
            if (value.type != "checkbox") {
                var hide = '';

                if (value.type == 'hidden') {
                    hide='hide'
                }
                var identifier;
                if (value.additionalName) {
                     
                    identifier = value.name + '_' + value.parent
                }
                else {
                    identifier=value.name
                }

                var div = '<div class="form-group ' + hide + ' ' + col_adjustment + value.class + '" id="' + identifier + '_field"></div>';

                var label = '<label class="control-label "' + col_adjustment2 + 'id="' + identifier + '_label">' + value.label + '</label>';


            $('#'+div2append).append(div);

           
                var input_div = label + '<div class="item-box input-group" id="' + identifier + '_input"></div>';

                $('#' + identifier + '_field').append(input_div);

                if (!value.addedClass) {
                    value.addedClass=""
                }

                var input = '<input class="form-control ' + value.addedClass + '" id="' + identifier + '"  style="width: 240px"></input>';

                $('#' + identifier + '_input').append(input);


                Object.keys(value).map(function (attributes,idx) {

                    if (value[attributes] != '' && attributes != 'adittionalHTLM') {
                        $('#' + identifier).attr(attributes, value[attributes]);
                    }
                    else {
                        if (attributes == 'adittionalHTLM') {
                            loadHTMLfunction(value.name,value[attributes])
                        }
                    }

                }) 
            }
            else {
                $('#'+ value.name + '_field').remove();
                var div_c = document.createElement('div');
                div_c.setAttribute('id', value.name + '_field');
                div_c.setAttribute('class','input-group-append')
                $('#serviceTab').append(div_c);

                var input = document.createElement('input');
                input.setAttribute('id', value.name + '_input');
                input.setAttribute('type', 'checkbox');

                Object.keys(value).map(function (params) {

                    if (typeof(value[params])==='object') {
                        value[params] = JSON.stringify(value[params])
                    }
                })

                input.setAttribute('onclick', 'validateState("' + value.name + '",' + value.action + ',"' + value.parent + '",' + ''+value.disableCheck + ')');


                $('#' + value.name + '_field').append(input);

                var label = document.createElement('label');
                label.setAttribute('id', value.name + '_label');
                label.innerHTML = value.label;
                $('#' + value.name + '_field').append(label);

                if (JSON.parse(value.state.toLowerCase())) {
                    document.getElementById(value.name + '_input').click();
                    //validateState(value.name, value.action, value.parent, value.disableCheck);
                }
                
            }

        }

    }
    else {

        var input = '<input id="' + value.name + '"></input>';

        $('#' + div2append).append(input);

        Object.keys(value).map(function (attributes) {

            $('#' + value.name).attr(attributes, value[attributes]);

        })
    }
}


function loadHTMLfunction(appended,Obj) {

    var F_elem = Obj.parent;
    var child_elem = Obj.child;
    var elem;
    var chelem;
    Object.keys(F_elem).map(function (parem) {
        if (parem=='type') {
            elem = document.createElement(F_elem[parem])
        }
        if (elem && parem != 'type') {
            elem.setAttribute(parem, F_elem[parem])
            $('#' + appended+'_input').append(elem)
            Object.keys(child_elem).map(function (children) {

                if (children == 'type') {
                    chelem = document.createElement(child_elem[children])
                    chelem.setAttribute(children, child_elem[children])
                }
                if (chelem && parem != 'type' && children!='text') {
                    chelem.setAttribute(children, child_elem[children])
                    if (elem.id) {
                        $('#' + elem.id).append(chelem)
                    }
                }
                if (children == 'text') {
                    chelem.innerHTML = child_elem[children];
                }
            })
        }
    })

}

function generateContract(constructor) {
    
    var subs = "REC";
    var d = new Date();
    var contract = subs+'FD'+ d.getFullYear().toString() + (d.getMonth() + 1).toString() + d.getDate().toString() + d.getHours().toString() + d.getMinutes().toString() + d.getSeconds().toString() + d.getMilliseconds().toString();
    $('#' + constructor.parentElement.parentElement.id.replace('_input','')).val(contract)
}

function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

function getDayWithoutTime() {
    var FullDate = '';

    var d = new Date();
    var year = d.getFullYear().toString();
    var month = d.getMonth()+1;

    if (month < 10) {
        month = '0' + month.toString();
    }
    else {
        month = month.toString();
    }

    var date = d.getDate()+1;

    if (date < 10) {
        month = '0' + date.toString();
    }
    else {
        date = date.toString();
    }

    FullDate = year +'-'+ month +'-'+ date;

    return FullDate;
}

function GetLocalConfig() {
    var client = new Object;
    client['language'] = "es_MX";
    client['country']="MX"
    return client;

}
// funci�n de mapeo de post
function arrJ(obj, res, par, child, _AddReq) {
    var result = {};
    var subres = {};
    if (!_AddReq) {
        _AddReq=[];
    }
    if (scheduleRequest.length > 1) {
        _obj = scheduleRequest;
    }
    else {
        _obj = [...obj]
    }
    var maxObj;
    var minObj;
    if (_obj.length > _AddReq.length) {
        maxObj = _obj;
        minObj = [..._AddReq];
    }
    else {
        maxObj = [..._AddReq];
        minObj = _obj;
    }
    ResObj=Object.assign(maxObj, minObj)
    //var final={}
        ResObj.map(function (Objv, i) {
            Object.values(Objv).map(function (Obv, j) {
                if (Objv.parent == Obv) {
                    if (Objv.name) {
                        result[Objv.name] = $('#' + Objv.name).val();
                    }
                    else {
                        if (Objv.value || Objv.function) {
                            if (Objv.value) {
                                result[Objv.parent] = Objv.value;
                            }
                            else {
                                var fn = window[Objv.function];
                                result[Objv.parent] = fn.apply(null, []);
                            }
                        }
                        else {
                            if (Objv.child) {
                                var child_element = $("[parent='" + Objv.child + "']");
                                var parent_element = $("[parent='" + Objv.parent + "']");

                                if (par != Objv.parent && parent_element.length >= 0 && child == Objv.parent) {

                                    if (par) {
                                        if (!result[par]) {
                                            if (!final[par] && !JSON.stringify(final).includes(par)) {
                                                final[par] = {};
                                            }
                                        }
                                        if (!subres[Objv.child]) {
                                            subres[Objv.child] = {};
                                        }

                                        parent_element.map(function (i, input) {

                                            subres[input.id] = $('#' + input.id).val().split(' ').join('');

                                        })

                                        child_element.map(function (i, input) {
                                            var field = input.id;

                                            if (input.id.includes('_'+Objv.child)) {
                                                field = field.replace('_' + Objv.child,'')
                                            }
                                            subres[Objv.child][field] = $('#' + input.id).val().split(' ').join('');
                                        })

                                        if (!final[par]) {
                                            Object.keys(final).map(function (k) {
                                                final[k][par][Objv.parent] = subres;

                                            })
                                        }
                                        else {
                                            final[par][Objv.parent] = subres;
                                        }
                                        
                                        
         
                                    }
                                }
                                else {
                                    if (!par) {
                                        arrJ(obj, {}, Objv.parent, Objv.child, _AddReq)
                                    }
                                     
                                }
                            }
                            else {
                                if (!result[Objv.parent]) {
                                    result[Objv.parent] = {};

                                }


                                var element = $("[parent='" + Objv.parent + "']");
                                var ele_l = element.length;
                                if (ele_l > 0) {
                                    element.map(function (i, input) {
                                        result[Objv.parent][input.id] = $('#' + input.id).val().split(' ').join('');;

                                    })

                                }
                                if (child) {
                                    result[Objv.parent][child] = {};

                                    var element = $("[parent='" + child + "']");
                                    var ele_l = element.length;
                                    if (ele_l > 0) {
                                        element.map(function (i, input) {
                                            result[Objv.parent][child][input.id] = $('#' + input.id).val().split(' ').join('');;

                                        })

                                    }
                                }
                            }
                            if (result[par]) {
                                final = result;
                            }
                        }
                    }
                }

            })
        })
    

    return Object.assign(result, final)
}


function validateCard(obj) {
    auth=false
    if (Object.keys(Req_Obj).length == 0) {
        Object.keys(jpost).map(function (key) {
            obj[key] = {};
            var ex_Obj = jpost[key];
            for (const i in ex_Obj) {
                if (i == 'number') {
                    obj[key][i] = $('#' + i).val().split(' ').join('');
                }
                else {
                    obj[key][i] = $('#' + i).val();
                }
            }

        })
        return obj;
    }
}
function getService() {
    validateAllStates()
    var servObj = new Object;

    var type_case = 'primary';
    if (create_token) {

        var type_case = 'secondary';
        var service = "Tokenization";
    }
    else {
        var service = "3ds"
    }
    servObj['service'] = service;
    if (auth) {
        servObj['key'] = "3ds"
    }
    servObj['type_case'] = type_case;

    return servObj;
}
function sendTrans() {

    var servObj = getService()

    var service = servObj['service']

    var type_case = servObj['type_case']

    postData($('#apikey').val(), $('#apisec').val(), Post_ReqGen(service), type_case, '', service);

    

}
function show_pwd(el) {
    if (el) {
        var elem = el.parentElement.parentElement.children[0];
        var id = el.parentElement.parentElement.children[0].id
        if (elem.type == "password") {
            elem.type = "text";
            $('#'+id+'_span').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
        } else {
            elem.type = "password";
            $('#' + id + '_span').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
        }
    }
}

$(document).ready(function () {
    load_config();
    show_pwd();
    $('#action').on('change', function () {
        var url = $('#action').val() + '/connect/gateway/processing';
        $('#form_udi').attr('action', url);
    });

    $('#hash_action').on('change', function () {
        $('#hash_algorithm').val($('#hash_action').val());
    });

    $('#type_of_selling').on('change', function () {
        load_config();
 
    });

});


function postData(apiKey, apiSec, payL, Case, transId, service, key) {

    if (key) {
        var valRef = document.getElementById('notificationReference');

        var key;
        if (valRef) {
            if (valRef.value) {
                key = 'RECEIVED'
            }
            else {
                key = 'EXPECTED_BUT_NOT_RECEIVED'
            }
        }
        else {
            key = 'EXPECTED_BUT_NOT_RECEIVED'
        }

        console.log(key)
        $('#notificationReference', window.parent.document).val('');
        if (payL['methodNotificationStatus']) {

            payL['methodNotificationStatus'] = key;
            payL['securityCode'] = $('#securityCode').val();
        }
    }

    var hashdata = new Object;

    var typeCase = Case;

    //payL["storeId"]="6202738"

    if (payL['order']) {
        referenceId = payL['order']['orderId'];
        if (payL['authenticationRequest']) {    
            payL['authenticationRequest']['methodNotificationURL'] = payL['authenticationRequest']['methodNotificationURL'] + '?reference=' + referenceId;
        }
    }

    var payload = JSON.stringify(payL);
    var clientId =  uuidv4();
    var timezone = Date.now();
    var message = apiKey + clientId + timezone + payload;
    var controller = sep_conf["HMACSHA256"].controller;
    
    hashdata.message = message;
    hashdata.sharedsecret = apiSec;


    var sign = MessageSignatureGen(hashdata, controller);


    post = Request_loader(post, sign, timezone, clientId, payload, typeCase, transId, service);

    ExecTrans(post, service);
    



};

function RecurringChange(params,state) {

    if (state) {
        if (params.parentChild) {
            scheduleRequest = params.parentChild;
            scheduleURL = params.NewEndPoint;
        } 
    }
    else {
        scheduleRequest = [];
    }
}

function AddStoretoRequest(params,state) {
    if (state) {
        setStore = true;
    }
    else {
        setStore = false;
    }
}

function AddTokentoOrder(params, state) {
    if (state) {
        setCrypto = true;
    }
    else {
        setCrypto = false;
    }
}

function storedCredentialsAdd(params, state) {
    if (state) {
        setCof = true;
    }
    else {
        setCof = false;
    }
}
function MessageSignatureGen(hashdata, controller) {

    var result=$.ajax({
        type: 'POST',
        url: '/HashExtended/' + controller + '/',
        data: hashdata,
        async: false,
        success: function (response) {
        }
    });

    return result.responseJSON.replace(/\"/g, "")
}


function validateAllStates() {
    $('[type=checkbox]').map(function (idx, states) {
        var state = document.getElementById(states.id).checked;
        if (state) {
            if (states.id == 'Tokenization_input') {
                create_token = true;

            }
            if (states.id != 'msi_input' && states.id != 'schedule_input' && states.id != 'multiMerchant_input' && states.id != 'pfac_input' && states.id != 'networktoken_input' && states.id != 'cof_input') {
                document.getElementById(states.id).onclick();
            }

        }
        else {
            if (states.id == 'Tokenization_input') {
                create_token = false;
            }
        }
    })

}
function Post_ReqGen(service,postToken,params,key) {
    validateAllStates();
    var paysend = {};

    if (create_token && !postToken) {
        paysend = Post_TokGen(service)
        ChangeURL(service, 'new', 'old');
        ChangeMethod(service, 'new', 'old', key)
       
    }
    else {
        if (create_token && postToken) {
            var ParGen = Sredirect[service].parentChild;
            var tokenValue = params['value'];
            $('#value').remove();
            var div = document.createElement('input');
            div.setAttribute('id', 'value');
            div.setAttribute('class', 'hide');
            div.setAttribute('parent', 'paymentToken');

            $('#div_master').append(div);
            ChangeParent(service, 'old', 'new');
            $('#value').val(tokenValue)
            var first = [...Req_Obj];

            paysend = Object.assign(first, ParGen)
            var additionalInputLen = $('#AdditionalReqTab').children().length;
            var addRequestObj = [];
            if (additionalInputLen > 0 && AddReq) {
                addRequestObj = [...AddReq];
            }
            paysend = arrJ(paysend, {}, undefined, undefined, addRequestObj);
            final = {}
            var servObj = getService()

            var service = servObj['service']
            $('#token_tic').modal('hide')
            if (Sredirect) {
                if (Sredirect[service]) {
                    if (Sredirect[service].split_trans) {
                        create_token = true;
                        ChangeURL(service, 'old', 'new');
                        ChangeMethod(service, 'old', 'new', key)
                        if (!paysend['order']) {
                            Object.assign(paysend, orderGen())
                        }
                        else {
                            Object.assign(paysend.order, orderGen().order)
                        }
                    }
                }
            }
        }
        else {

            if (Object.keys(Req_Obj).length > 0) {
                var additionalInputLen = $('#AdditionalReqTab').children().length;
                var addRequestObj = [];
                if (additionalInputLen > 0 && AddReq) {
                    addRequestObj = [...AddReq];
                }
                paysend = arrJ(Req_Obj, {}, undefined, undefined, addRequestObj);
                final = {}
                if (!paysend['order']) {
                    Object.assign(paysend, orderGen())
                }
                else {
                    Object.assign(paysend.order, orderGen().order)
                }
            }
            else {
                paysend = validateCard({});
            }
        }

        if (auth) {
            var authReq = {};
            authReq[authParent] = {};
            $("[parent='" + authParent + "']").map(function (index, input) {

                if (input.getAttribute('concat_type') == 'URL') {
                    var val_in =window.location.origin + $('#' + input.id).val();

                }
                else {
                    var val_in = $('#' + input.id).val();
                }

                authReq[authParent][input.id] = val_in;

            })
            /*
            paysend["transactionAmount"]["components"] = {};
            paysend["transactionAmount"]["components"]["subtotal"] = paysend["transactionAmount"]["total"]
            paysend["transactionAmount"]["total"] = 10 + parseInt(paysend["transactionAmount"]["components"]["subtotal"])
            paysend["transactionAmount"]["components"]["vatAmount"] = 10;*/
            Object.assign(paysend, authReq);

        }
    }
    var state = document.getElementById('Preauth_input')
    if (state) {
        state=state.checked;
        isPreauthfunction('Preauth', state, paysend)
    }
    if (setStore) {
        paysend.storeId = $('#storeId').val();
    }

    if (setCrypto) {
        paysend.order['tokenCryptogram'] = $('#cryptogram').val();
    }

    if (setCof) {
        paysend['storedCredentials'] = {};
        paysend['storedCredentials']['sequence'] = $('#cofsequence').val();
        paysend['storedCredentials']['scheduled'] = "false"
        paysend['storedCredentials']['indicatorSubcategory'] = "UNSCHEDULED_CREDENTIAL_ON_FILE"
        paysend['storedCredentials']['initiator'] = $('#initiator').val();
        if (paysend['storedCredentials']['initiator'] == "MERCHANT") {
            paysend['storedCredentials']['referencedSchemeTransactionId'] = $('#referencedSchemeTransactionId').val();
        }
        
    }

    return paysend;

}


function Request_loader(params, message, time, client, request, typeCase, transId, service) {

    params.headers = {};

    heads = headerGen(typeCase, service);

    if (transId != '') {
        transId = '/' + transId;
    }

    if (scheduleRequest.length   > 1) {
        params.url = $('#action').val() + scheduleURL + transId;
    }
    else {
        params.url = $('#action').val() + heads.url + transId;
    }

    params.Httpmethod = heads.Httpmethod;

    params.async = heads.async;
    
    params.controller = heads.controller;

    params.url_params = heads.param;

    params.headers["Accept"] = heads.content

    params.headers["Content-type"] = heads.content

    params.Payload = request;

    params.headers["Api-Key"] = $('#apikey').val();

    params.headers["Client-Request-Id"] = client;

    params.headers["Timestamp"] = time;

    params.headers["Message-Signature"] = message;

    return params;

}

function headerGen(typeCase, service) {

    if (typeCase == 'primary') {

        return redirect;

    }
    else {
        return Sredirect[service];
    }
}

function ExecTrans(params, service,key) {

    var data = {};

    data.post = JSON.stringify(params);

    if (validateCardNumber($('#number').val())) {
        $('#bckgloader').removeClass('hide');
        $.ajax({
            type: params.Httpmethod,
            url: window.location.origin + '/' + params.controller + '/' + params.url_params,
            dataType: 'json',
            data: data,
            error: function (response) {
            },
            success: function (response) {

                if (JSON.parse(response)) {
                    if (JSON.parse(response).Code == 'Error') {
                        $('#threeds_tic', window.parent.document).removeClass('hide')
                        FailurePage(response);
                        $('#bckgloader').addClass('hide');
                        final = {};
                    }
                    else if (!JSON.parse(response).Code) {
                        if (!create_token) {
                            SuccessPage(response, onComplete, 'success')
                            $('#bckgloader').addClass('hide');
                        }
                        else {
                            var val;
                            var Obj_st;
                            if (JSON.parse(response).transactionStatus== "APPROVED") {
                                val = 'success';
                                Obj_st = onComplete;
                            }
                            else {
                                val = 'token'
                                Obj_st = SplitComplete;
                            }
                            SuccessPage(response, Obj_st, val)
                            $('#bckgloader').addClass('hide');
                        }
                        final = {};
                    }
                    else {
                        threedsAuthProcessor(response, service);
                        final = {}
                    }

                }
                else {
                    FailurePage('{"Message":"Ocurrio un error desconocido"}');
                    $('#bckgloader').addClass('hide');
                }
            }
        })
    }
    else {
        FailurePage('{"Message":"No. Tarjeta no valida"}');
        $('#bckgloader').addClass('hide');
    }
}

function yearArrayLoader() {

    var yarray = [];
    for (var i = 0; i < 30; i++) {
        var date = new Date();
        var year = date.getFullYear() + i
        var value = (date.getFullYear() + i).toString().substr(-2);
        if (value.length < 2) {
            value = '0' + val;
        }
        var dateobj = new Object;
        dateobj.id = year;
        dateobj.label = year;
        dateobj.value = value;
        yarray.push(dateobj)
    }

    return yarray;
}

function monthArrayLoader() {
    var marray = [];
    for (var i = 0; i < 12; i++) {
        var date = new Date(2020, i, 1);
        var month = date.toLocaleString('default', { month: 'long' });
        var mobj = new Object();
        mobj.id = month;
        mobj.label = month;
        var val = (i + 1).toString();
        if (val.length < 2) {
            val = '0' + val;
        }
        mobj.value = val;
        marray.push(mobj)
    }

    return marray;

}

function patchLoader(code,vers, transId,service,key) {


    patch = getPatch(code, vers, version[vers].request);

    if (setStore) {
        patch.storeId = $('#storeId').val();
    }
    else {
        delete patch.storeId
    }

    patch['authenticationType'] = version[vers].ReqVal;


    if (key) {
        
        setTimeout(function () {
           
            postData($('#apikey').val(), $('#apisec').val(), patch, 'secondary', transId, service, key)
        }, 6000)
    }
    else {
        postData($('#apikey').val(), $('#apisec').val(), patch, 'secondary', transId, service, key)
    }


}

function threeDsformLoader(jform, response) {
    var htform;
    var input = "";
    var html;

    Object.keys(response).map(function (resp) {
        jform.map(function (form) {
            if (form.type == 'form') {

                htform = '<form action="' + response[form["action"]] + '" target="threeds_tic" method="POST" id="formAcs">'
                
            }
            else {

                if (response[form["value"]] && resp == form["value"]) {
                    input = input+'<input name="' + form["name"] + '" id="'+form["name"]+'" type="hidden" value="' + response[form["value"]] + '">'

                }
                
            }
            
        })
        html = htform + input + '</form>'

    })
    return html;
}

function orderGen() {

    var order = "FD";
    var d = new Date();
    var oid_str = d.getFullYear().toString() + (d.getMonth() + 1).toString() + d.getDate().toString() + d.getHours().toString() + d.getMinutes().toString() + d.getSeconds().toString() + d.getMilliseconds().toString();
    order = order + oid_str;

    if (scheduleRequest.length > 1) {

        return { "orderId": order };
    }
    else {
        orderObj.order = { "orderId": "" };
        orderObj.order.orderId = order;
        return orderObj;
    }
}

function GenerateReportCSV(params,values, csvArr) {

    var csvRow = new Object;

    var ValJ = JSON.parse(values)
    var sub_msj = ''
    var msj = ''
    var keyed;
        params.key.map(function (getVal) {

            var subObj = ValJ[getVal];
            if (typeof (subObj) != 'string' && subObj) {
                params.key.map(function (arr, i) {
                    if (typeof (arr) != 'string') {
                        arr.map(function (arrO,j) {
                            msj = msj + subObj[arrO]
                        })
        
                        if (!csvRow[params.Column]) {
                            if (!(params.Column in csvArr)) {
                                csvRow[params.Column] = msj;
                                csvArr.push(csvRow)
                            }
                            
                        }
                    }
                })

            }
            else {
                if (ValJ[getVal]) {
                    if (!(params.Column in csvArr)) {
                        csvRow[params.Column] = ValJ[getVal]
                        csvArr.push(csvRow)
                    }
                }
            }

            
        })

    return csvArr;
   
    
}

function AddAdditionalServices(Arr) {


    onSave.map(function (save) {
        var additionalVal = new Object;
        additionalVal[save.text] = save;
        Arr.push(additionalVal)

    })

    return Arr;

}

function SuccessPage(params, completeObj, tic) {


    var Completed = completeObj;
    $('modal_' + tic + '_id').children().remove()
    $('#' + tic+'_tic').modal('show')
    var csvArr = [];
    Completed.map(function (ret) {
        if (ret.save) {
            GenerateReportCSV(ret, params, csvArr);
        }
        if (ret.type != 'button') {
            $('#txn' + ret.id + '_header').remove();
            $('#txn' + ret.id).remove();
            var message_str = document.createElement(ret.type);
            var msj_div = document.createElement('div');
            msj_div.setAttribute('id', 'txn' + ret.id);
            message_str.setAttribute('id', 'txn' + ret.id + '_header');
            $('#modal_'+tic+'_id').append(msj_div);
            if (ret.label) {
                var message = ret.label;
                if (ret.key) {


                    //message_str.innerHTML = ret.label
                    var Obj;
                    var msj;
                    var sub_msj = "";
                    ret.key.map(function (keys) {


                        var subObj = JSON.parse(params)[keys];
                        if (subObj) {
                            Obj = subObj
                            msj = subObj;
                            //message_str.innerHTML = message + msj;
                        }

                        if (typeof (keys) != 'string' && Obj) {
                            keys.map(function (arr, i) {
                                sub_msj = sub_msj + Obj[arr]
                                if (i === keys.length - 1) {

                                    msj = sub_msj;
                                }
                            })

                        }
                        if (msj) {
                            message_str.innerHTML = message + msj;
                        }
                    })

                }
                else {
                    message_str.innerHTML = message;
                }

            }

            $('#txn' + ret.id).append(message_str);
        }
        else {
            $('#' + ret.id + '_btn').remove();
            var msj_div = document.createElement('div');
            msj_div.setAttribute('id', ret.id + '_btn');
            $('#modal-' + tic + '-content').append(msj_div);
            var service = getService()
            
            if (auth) {
               var key = JSON.stringify(service['key'])
            }
            var txn_button = '<button id="' + ret.id + '" type="button" onclick=' + ret.function + '(' + JSON.stringify(JSON.parse(params)[ret.key]) + ',' + JSON.stringify(service['service']) + ','+key+') class="center col-sm-12 btn-' + ret["color-type"] + '">' + ret.label + '</button>'
            $('#' + ret.id+'_btn').append(txn_button);
        }

    })
    //var fullArr = AddAdditionalServices(csvArr);
    //SaveJReport(fullArr);
}

function SaveJReport(csv) {
    
    $.ajax({
        type: 'POST',
        url: '/Report/GenerateFile/',
        data: csv,
        async: false,
        success: function (response) {
            console.log(csv)
        }
    })

}

function close_modal() {
   
    $('#threeds_tic', window.parent.document).modal('hide')
    $('#threeds_tic', window.parent.document).addClass('hide')
    var range = document.createRange();
    range.selectNodeContents(document.getElementById("modal_success_id"));
    range.selectNodeContents(document.getElementById("modal_error_id"));
    range.selectNodeContents(document.getElementById("modal_token_id"));
    range.deleteContents();
    $("#success_tic").modal('hide');
    $("#error_tic").modal('hide');
    $("#token_tic").modal('hide');
}

function FailurePage(params) {
    $('#txnerror').remove();
    var msj = JSON.parse(params).Message
    var msj_div = document.createElement('h2');
    msj_div.setAttribute('id', 'txnerror');
    $('#modal_error_id').append(msj_div);
    $("#error_tic").modal('show');
    msj_div.innerHTML = msj;

}

const validateCardNumber = number => {
    //Check if the number contains only numeric value  
    //and is of between 13 to 19 digits
    number=number.split(' ').join('');
    const regex = new RegExp("^[0-9]{13,19}$");
    if (!regex.test(number)) {
        return false;
    }

    return luhnCheck(number);
}

const luhnCheck = val => {
    let checksum = 0; // running checksum total
    let j = 1; // takes value of 1 or 2

    // Process each digit one by one starting from the last
    for (let i = val.length - 1; i >= 0; i--) {
        let calc = 0;
        // Extract the next digit and multiply by 1 or 2 on alternative digits.
        calc = Number(val.charAt(i)) * j;

        // If the result is in two digits add 1 to the checksum total
        if (calc > 9) {
            checksum = checksum + 1;
            calc = calc - 10;
        }

        // Add the units element to the checksum total
        checksum = checksum + calc;

        // Switch the value of j
        if (j == 1) {
            j = 2;
        } else {
            j = 1;
        }
    }

    //Check if it is divisible by 10 or not.
    return (checksum % 10) == 0;
}

function Post_TokGen(service) {

    //Generamos Token aqu�
    var gen = Sredirect[service].subform.parentchild;

    var tok_req = {};

    var comp = {};

    SplitComplete = Sredirect[service].subform.onComplete;

    comp = Sredirect[service].subform.complementObj;

    var tokenValue = 'RT' + orderGen().order.orderId;

    Sredirect[service].subform.complementObj.createToken.value=tokenValue;

    tok_req = arrJ(gen, {}, undefined, undefined);
    final = {};

    return Object.assign(tok_req, comp);

}

function PayWithToken(params,service,key) {
    ChangeParent(service, 'old', 'new');
    ChangeURL(service, 'new', 'old');
    postData($('#apikey').val(), $('#apisec').val(), Post_ReqGen(service, true,params), 'primary', '', service,key);
    //Falta modificar la url del endpoint y listo

}

function ChangeParent(val, old, changed) {
    if (Sredirect[val]) {
        var conf = Sredirect[val].parentChange;
        if (conf) {
            conf.map(function (change) {
                $('[parent=' + change[old] + ']').map(function (index, child) {
                    if (child.id == change.child) {

                        document.getElementById(child.id).setAttribute('parent', change[changed])
                    }
                })
            })
        }
    }
}

function ChangeURL(val, old, changed) {
    var conf = Sredirect[val].url_change;
    if (conf) {
        conf.map(function (change) {
            if (Sredirect[val].url == change[old]) {
                Sredirect[val].url = change[changed]
            }
        })
    }
}

function ChangeMethod(val,old,changed) {
    var conf = Sredirect[val].HttpmethodChange;
    var cont = Sredirect[val].ControllerChange;
    conf.map(function (change) {
        if (Sredirect[val].Httpmethod == change[old]) {
            Sredirect[val].Httpmethod = change[changed]
        }
    })
    cont.map(function (change) {
        if (Sredirect[val].param == change[old]) {
            Sredirect[val].param = change[changed]
        }
    })
}

function isPreauthfunction(val,state,obj) {

        if (val == "Preauth") {
            services_conf.map(function (getVal) {

                if (getVal[val]) {
                    var change = getVal[val].requestChange
                    //onComplete = getVal[val].onComplete;
                    change.map(function (transformed) {
                                if (obj.requestType.includes(transformed.key) && !obj.requestType.includes(transformed.ignore)) {
                                if (state) {
                                    obj.requestType = transformed.newkey + transformed.final;
                                }
                                else {
                                    obj.requestType  = transformed.key + transformed.final;
                                }
                            }

                })
                }
            })
        }
    
}

function loadAdditionalInputs(val,state) {

        services_conf.map(function (service) {
            var config = service[val]
            if (config) {
                if (config.formLoader) {
                    var loader_arr = config.formLoader

                    loader_arr.map(function (loader) {
                        var exists = loader.name;
                        var createEl = false;
                        if (!document.getElementById(exists) || loader.additionalName) {
                            createEl=true;
                        }
                        
                        if (createEl && state) {
                            var newService = document.createElement('div');
                            newService.setAttribute('id', val + '_service');
                            newService.setAttribute('service', val);
                            if (!$('#' + val + '_service')[0]) {
                                $("#AdditionalReqTab").append(newService);
                            }
                            input_loader(loader, val + '_service')

                            var maxObj;
                            var minObj;

                            if (config.parentChild.length > AddReq.length) {

                                maxObj = config.parentChild;
                                minObj = AddReq

                            }
                            else {
                                maxObj = AddReq
                                minObj = config.parentChild;
                            }
                            let merge = (a, b, p) => a.filter(aa => !b.find(bb => aa[p] === bb[p]));
                            AddReq = maxObj.concat(merge(minObj, maxObj, "parent"));
                        }
                        else {
                            $('#' + val + '_service').children().remove()
                            AddReq = [];
                        }
                    })

                }
            }
        }
        )
    


}

function changeValueReq(action, state) {

    if (typeof (action) != 'object') {
        var ObjAction = JSON.parse(action);
    }
    else {
        ObjAction=action
    }

    if (state) {
        

        $('#' + ObjAction.Key).val(ObjAction.NewValue)
    }
    else {
        $('#' + ObjAction.Key).val(ObjAction.OldValue)
    }
}

function omitInputValue(params, state) {

    if (state) {
        params.inputLoads.map(function (find) {

            $('#' + find + '_field').addClass('hide');
            document.getElementById(find).setAttribute('type', 'hidden')
            $('#' + find).val('null')
        })

    }
    else {
        params.inputLoads.map(function (find) {
            $('#' + find + '_field').removeClass('hide');

            document.getElementById(find).setAttribute('type', 'password')

            $('#' + find).val('')
        })
    }
}

function validateState(val, action, parent, disableCheck) {
    
    ChangeParent(val, 'new', 'old');

    if (val.includes('_input')) {
        var state = document.getElementById(val).checked;
    }
    else {
        var state = document.getElementById(val + '_input').checked;
    }



    t_request = {};
    

    if (action) {
        
        action.map(function (acts) {
            var Arrpar = [...acts.params];
            var fn = window[acts.function];
            Arrpar.push(state)
            if (typeof fn === "function") fn.apply(null, Arrpar);
        })
    }
    if (state) {

        if (disableCheck) {
            disableCheck.map(function (dis) {
                if (document.getElementById(dis + '_input')) {
                    if (document.getElementById(dis + '_input').checked) {
                        document.getElementById(dis + '_input').click();
                    }
                }

            })

        }
        //tokenization
        if (Sredirect) {
            if (Sredirect[val]) {
                if (Sredirect[val].split_trans) {
                    create_token = true;

                }
                else {
                    create_token = false;
                    ChangeParent(val, 'new', 'old');
                    
                }
            }
        } 
        $("[master='" + val + "']").map(function (index, cls) {

            $('#' + cls.id).removeClass('hide')
            auth = true; authParent = parent;       

        })
    }
    else {

        if (Sredirect.split_trans) {
            create_token = false;
            ChangeParent(val, 'new', 'old');
        }
        $("[master='" + val + "']").map(function (index, cls) {

            $('#' + cls.id).addClass('hide')
            auth = false;

        })

    }
    loadAdditionalInputs(val, state);
}

function threedsAuthProcessor(params, service) {
    
    if (JSON.parse(params).Code == 'FINALIZING') {
        $('#bckgloader').addClass('hide');
        $('#threeds_tic', window.parent.document).removeClass('hide')
        $('#threeds_tic').modal({ backdrop: 'static', keyboard: false })
        var resp_auth = JSON.parse(JSON.parse(params).Message);
        var vs = JSON.parse(params).Version
        //generamos el form de la respuesta del PATCH
        var form = version[vs].form;
        var requestArr = version[vs].request;
        //Mapeamos respuesta 
        var final_form = threeDsformLoader(form, resp_auth,requestArr);
        $('#div_master').append(final_form)
        document.getElementById('formAcs').submit();
        $('#threeds_tic').modal('show');
        //Mandamos al iframe los datos como header,ipgtransactionId y completar la transaccio�n
        authpatch = getPatch(JSON.parse(params).Code, JSON.parse(params).Version, requestArr);
        var mapper = version[vs].resultMapper
        post.Payload = authpatch;
        post.url_params = 'PatchTransaction';
        post.url = $('#action').val() + heads.url + '/' +JSON.parse(params).TransId;;
        post.Payload.securityCode = $('#securityCode').val();
        if (setStore) {
            post.Payload.storeId = $('#storeId').val();
        }
        else {
            delete post.Payload.storeId
        }
        $('#threeds_tic_id').attr('obj', JSON.stringify(post))
        $('#threeds_tic_id').attr('mapper', JSON.stringify(mapper))
        $('#threeds_tic_id').attr('complete', JSON.stringify(onComplete))
        $('#formAcs').remove();
        final = {};
    }
    else {
        document.getElementById('authframe').innerHTML = ""
        $('#threeds_tic').modal({ backdrop: 'static', keyboard: false })
        var iframeauth = JSON.parse(JSON.parse(params).Message).methodForm;
        var vers = JSON.parse(params).Version;
        var transactionId = JSON.parse(params).TransId;
        $('#authframe').append(iframeauth);
        $('#bckgloader').removeClass('hide');
        patchLoader(JSON.parse(params).Code, vers, transactionId, service,true);
        final = {};
        
    }
}

function getPatch(code,version,request) {
    var result;
    request.map(function (req) {
        if (req[code]) {
            result = req[code];
        }
    });
    return result;
}