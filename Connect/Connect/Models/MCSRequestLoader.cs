using System.Xml.Serialization;

namespace WebApplication1.Models
{
    [XmlRoot("Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class BaseSoapRequest
    {
        [XmlElement("Body")]
        public RequestBody Body { get; set; }
    }

    [XmlRoot("Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class RequestBody
    {
        [XmlElement("mcsRequest", Namespace = "http://www.ipg-online.com/mcsWebService")]
        public MCSRequest Request { get; set; }
    }

    [XmlRoot("Body", Namespace = "http://www.ipg-online.com/mcsWebService")]
    public class MCSRequest
    {
        [XmlElement(ElementName = "getStore", IsNullable = false)]
        public GetStoreRequest GetStoreRequest { get; set; }
    }

    [XmlRoot("getStore")]
    public class GetStoreRequest
    {
        [XmlElement("storeID")]
        public string StoreId { get; set; }
    }
}