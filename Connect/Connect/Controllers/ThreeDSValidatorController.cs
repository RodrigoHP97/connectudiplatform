using Newtonsoft.Json;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Controllers
{
    public class ThreeDSValidatorController : Controller
    {
        public IActionResult Notification()
        {

            var list = new Dictionary<string, string>();
            list.Add("reference", Request.Query["reference"]);

            try
            {
                foreach (string key in Request.Form.Keys)
                {
                    list.Add(key, Request.Form[key]);
                }
                var data = Json(list).Value;
                ViewBag.data = JsonConvert.SerializeObject(data);
            }
            catch
            {
                list.Add("threeDSMethodData", "");
            }

            return View();
        }
        [HttpPost]
        public IActionResult Authentication()
        {
            var list = new Dictionary<string, string>();
            foreach (string key in Request.Form.Keys)
            {
                list.Add(key, Request.Form[key]);
            }
            var data = Json(list).Value;
            ViewBag.data = JsonConvert.SerializeObject(data);

            return PartialView("~/Views/ThreeDSValidator/Authentication.cshtml");
        }
    }
}
