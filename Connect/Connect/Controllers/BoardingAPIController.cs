using System;
using System.IO;
using RestSharp;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using WebApplication1.Models;
using System.Xml;
using System.Security.Cryptography.X509Certificates;

namespace WebApplication1.Controllers

{

    public class BoardingAPIController : Controller
    {
        public string CloneParameter(string xmlstring, string cloneStore)
        {

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlstring);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ns2", "http://www.ipg-online.com/mcsWebService");
            nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            XmlNode XMLchangedList = doc.DocumentElement.SelectSingleNode("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:data/ns2:store", nsmgr);

            foreach (XmlNode XmlList in XMLchangedList)
            {
                if (XmlList.LocalName == "storeID")
                {
                    XmlList.InnerText = cloneStore;

                }
                if (XmlList.LocalName == "storeAdmin")
                {
                    XmlList["ns2:id"].InnerText = cloneStore;

                }

            }
            string XMLchangedStore = doc.DocumentElement.SelectNodes("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:data/ns2:store", nsmgr)[0].OuterXml;

            return XMLchangedStore;
        }

        public string UpdateParameter(string xmlstring) {

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlstring);

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ns2", "http://www.ipg-online.com/mcsWebService");
            nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            XmlNode XMLchangedList = doc.DocumentElement.SelectSingleNode("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:data/ns2:store", nsmgr);

            foreach (XmlNode XmlList in XMLchangedList)
            {
                if (XmlList.LocalName == "service" && XmlList["ns2:type"].InnerText == "3dSecure")
                {
                    foreach (XmlNode XmlParams in XmlList)
                    {

                        if (XmlParams.InnerText != "3dSecure")
                        {
                            if (XmlParams["ns2:item"].InnerText == "integration")
                            {
                                XmlParams["ns2:value"].InnerText = "Modirum3DSServer";
                            }
                        }
                    }

                    //   XmlList["ns2:config"]["ns2:value"].InnerText = "Modirum3DSServertest";


                }

            }
            string XMLchangedStore = doc.DocumentElement.SelectNodes("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:data/ns2:store", nsmgr)[0].OuterXml;

            return XMLchangedStore;
        }

        
        public string SendBoardingRequest(string Body, string reseller) {

            var certFile = Path.Combine(@"../Connect/wwwroot/certs/"+ reseller, reseller+".p12");
            var cerData = getResellerData(reseller);
            var JCert=JsonConvert.DeserializeObject<Dictionary<string, string>>(cerData.Value.ToString());
            string certPwd = JCert["CertPwd"];
            var user = JCert["APIUser"]; ;
            var pwd = JCert["APIPassword"]; ;
            X509Certificate2 certificate = new X509Certificate2(certFile, certPwd);
            var client = new RestClient("https://www2.ipg-online.com/mcsWebService");
            client.ClientCertificates = new X509CertificateCollection() { certificate };
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "text/xml");
            client.Authenticator = new HttpBasicAuthenticator(user, pwd);
            //request.AddHeader("Authorization", client.Authenticator.ToString());
            request.AddHeader("CERT_REMOTE_USER", user);

            request.AddParameter("text/xml", Body, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(response.Content.ToString());
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ns2", "http://www.ipg-online.com/mcsWebService");
            nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            XmlNode XMLEval = doc.DocumentElement.SelectSingleNode("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:result", nsmgr);

            if (XMLEval.InnerText == "SUCCESS")
            {
                return response.Content.ToString();
            }
            else {
                return "Error";
            }
        }

        public JsonResult getResellerData(string reseller) {
            using (StreamReader r = new StreamReader("../Connect/wwwroot/certs/"+reseller+"/" +reseller + ".json"))
            {
                string json = r.ReadToEnd();
                return Json(json);
            }
        }

        public string GetStoreParam(string storeXML, string keyPar) {
            string result = "";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(storeXML);
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            nsmgr.AddNamespace("ns2", "http://www.ipg-online.com/mcsWebService");
            nsmgr.AddNamespace("SOAP-ENV", "http://schemas.xmlsoap.org/soap/envelope/");
            if (keyPar == "FULL")
            {
                var strData = "";
                XmlNode XmlName = doc.DocumentElement.SelectSingleNode("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:data/ns2:store/ns2:legalName", nsmgr);
                XmlNode XmlMerchantId= doc.DocumentElement.SelectSingleNode("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:data/ns2:store/ns2:terminal/ns2:externalMerchantID", nsmgr);
                XmlNode XmlTerminalId = doc.DocumentElement.SelectSingleNode("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:data/ns2:store/ns2:terminal/ns2:terminalID", nsmgr);
                XmlNode XMLchangedList = doc.DocumentElement.SelectSingleNode("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:data/ns2:store", nsmgr);
                strData = strData + XmlName.InnerXml +"," + XmlMerchantId.InnerXml + "," + XmlTerminalId.InnerXml;
                foreach (XmlNode XmlList in XMLchangedList)
                {
                    if (XmlList.LocalName == "service" && XmlList["ns2:type"]!= null) {

                        if (XmlList["ns2:type"].InnerText != null) {

                            if (XmlList["ns2:type"].InnerText == "connect")
                            {
                                foreach (XmlNode XmlParams in XmlList)
                                {

                                    if (XmlParams.InnerText != "connect")
                                    {
                                        if (XmlParams["ns2:item"].InnerText == "sharedSecret")
                                        {
                                            string XmlSharedSecret = XmlParams["ns2:value"].InnerText;
                                            strData = strData + "," + XmlSharedSecret;
                                            result = strData;
                                        }

                                    }
                                }

                                //   XmlList["ns2:config"]["ns2:value"].InnerText = "Modirum3DSServertest";


                            }
                        }

                    }
                    else
                    {
                        result = strData;
                    }
                    }
                    {


                }
                

            }
            else
            {
                XmlNode XMLEval = doc.DocumentElement.SelectSingleNode("//SOAP-ENV:Envelope/SOAP-ENV:Body/ns2:mcsResponse/ns2:data/ns2:store/ns2:terminal/ns2:" + keyPar, nsmgr);
                result= XMLEval.InnerXml;
            }
            return result;
        }

        [HttpPost]
        public JsonResult UpdateStore(XMLrequest xmlrequest)  {

            string XMLResponse = SendBoardingRequest(xmlrequest.request, xmlrequest.reseller);//

            var result = new JRequestResponse();

            if (XMLResponse != "Error")
            {

                result.Status = "UPDATED";
            }
            else
            {
                result.Status = "FAILED";
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult CreateStore(XMLrequest xmlrequest)
        {
            string storeParams = SendBoardingRequest(xmlrequest.request, xmlrequest.reseller);//
            var result = new JRequestResponse();


            result.XMLResponse = storeParams;


            if (storeParams != "Error")
            {

                result.Status = "CLONED";
            }

            else
            {
                result.Status = "FAILED";
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult GetStore(XMLrequest xmlrequest)
        {
            string storeParams = SendBoardingRequest(xmlrequest.request,xmlrequest.reseller);//
            var result = new JRequestResponse();

            if (xmlrequest.update == true)
            {
                result.XMLResponse = UpdateParameter(storeParams);
            }
            else
            {
                if (xmlrequest.clone == true)
                {
                    string clonedStore = xmlrequest.clonedStore;
                    result.XMLResponse = CloneParameter(storeParams, clonedStore);
                }
                else
                {
                    if (xmlrequest.keyParam != null || xmlrequest.keyParam == "")
                    {
                        string paraRes = GetStoreParam(storeParams, xmlrequest.keyParam);

                        result.GotParam = paraRes;
                    }
                    else
                    {
                        result.XMLResponse = storeParams;
                    }
                }
            }

            if (storeParams != "Error")
            {

                result.Status = "GOT";
            }

            else
            {
                result.Status = "FAILED";
            }

            return Json(result);
        }

        [HttpPost]
        public JsonResult ResellerConfig()
        {

            using (StreamReader r = new StreamReader("../Connect/wwwroot/Configurations/MCSWS/Resellers.json"))
            {
                string json = r.ReadToEnd();
                return Json(json);
            }
        }
    }


}


public class XMLrequest
{
    public string request
    {
        get;
        set;
    }
        public bool update
    {
        get;
        set;
    }
    public string reseller
    {
        get;
        set;
    }
    public string keyParam
    {
        get;
        set;
    }

    public bool clone
    {
        get;
        set;
    }

    public string clonedStore
    {
        get;
        set;
    }
}

public class JRequestResponse
{
    public string XMLResponse { get; set; }
    public string Status { get; set; }

    public string GotParam { get; set; }

}
